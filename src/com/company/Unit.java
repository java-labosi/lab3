package com.company;

public class Unit {
    private int health;
    private int mana;

    public Unit(){
        this.health=100;
        this.mana=100;
    }

    public int getHealth(){
        return this.health;
    }
    public int getMana(){
        return this.mana;
    }
    public void getOut(){
        System.out.printf("health: %d, mana: %d",health, mana);
    }

}

class Destroyer extends Unit{
    private String name;
    private Weapon weapon;
    public Destroyer(){
        super();
        this.name="Destroyer";
    }
    public Destroyer(int i){
        super();
        this.name="Destroyer";
        weapon=new Weapon();
    }
    @Override
    public void getOut() {
        super.getOut();
        System.out.printf(" name: %s",name);
    }
    public void getOut(int n){
        super.getOut();
        System.out.printf(" name: %s, weapon: %s",name,weapon.getWeapon());


    }
}

class Weapon{
    private String wep;
    public Weapon(){
        this.wep="Bazooka";
    }
    public String getWeapon(){
        return this.wep;
    }
}